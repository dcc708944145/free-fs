package com.free.fs.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ResultEnum {
    RESULT_SUCCESS(200),

    RESULT_FAIL(500);
    /**
     * Request 类型
     */
    private final int code;


    public static ResultEnum find(int t_code) {
        for (ResultEnum value : ResultEnum.values()) {
            if (value.code == t_code) {
                return value;
            }
        }
        return RESULT_SUCCESS;
    }
}
