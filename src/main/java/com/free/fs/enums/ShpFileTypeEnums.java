package com.free.fs.enums;

public enum ShpFileTypeEnums {
    sbx(".sbx"),
    prj(".prj"),
    sbn(".sbn"),
    shp(".shp"),
    shx(".shx");

    private String str;

    ShpFileTypeEnums(String str) {
        this.str = str;
    }

    @Override
    public String toString() {
        return str;
    }
}
