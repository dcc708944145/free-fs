package com.free.fs.service;

import com.free.fs.common.response.HandResult;
import org.springframework.web.multipart.MultipartFile;

public interface InitDataImpl {
    HandResult uploadZip(MultipartFile file) throws Exception;

    HandResult unZip(String zipDir, String saveDir) throws Exception;

    HandResult init(String tableName, String shpDir) throws Exception;

    HandResult clipRaster(String layer, String tableName, int gid, double pixelX, double pixelY);
}
