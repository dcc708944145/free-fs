package com.free.fs.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.free.fs.common.response.HandResult;
import com.free.fs.common.tools.InitShp;
import com.free.fs.common.tools.OperationFile;
import com.free.fs.enums.ShpFileTypeEnums;
import com.free.fs.service.InitDataImpl;
import com.free.fs.utils.raster.ClipRaster;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Service
public class InitDataService implements InitDataImpl {
    @Autowired
    InitShp initShp;
    @Autowired
    ClipRaster clipRaster;

    @Override
    public HandResult uploadZip(MultipartFile file) throws Exception {
        String zipDir = String.format("%s/data/zip", System.getProperty("user.dir"));
        String shpDir = String.format("%s/data/shp", System.getProperty("user.dir"));
        OperationFile operationFile = new OperationFile();
        //如果保存zip压缩包的目录不存在则新建目录
        cleanFile(operationFile, zipDir);
        cleanFile(operationFile, shpDir);
        //上传的文件为空则返回失败
        if (file.isEmpty()) {
            return HandResult.FAIL("上传的是空文件");
        }
        Path zipFile = Paths.get(zipDir).resolve(file.getOriginalFilename());
        try {
            // 保存文件到指定文件夹
            Files.copy(file.getInputStream(), zipFile);
            List<String> fileNames = operationFile.readZipFileName(zipFile.toString());
            if (fileNames.size() == 0) {
                return HandResult.FAIL("压缩包中文件名称为中文");
            }
            //缺少的类型
            List<String> lackTypes = new ArrayList<>();
            lack(fileNames, ShpFileTypeEnums.prj.toString(), lackTypes);
            lack(fileNames, ShpFileTypeEnums.shx.toString(), lackTypes);
            lack(fileNames, ShpFileTypeEnums.shp.toString(), lackTypes);
            if (lackTypes.size() > 0) {
                return HandResult.FAIL(lackTypes.toString());
            } else {
                unZip(zipFile.toString(), shpDir);
//                String tableName = String.format("tc_bl_%s", date());
                String tableName = "tc_bl";
                init(tableName, shpDir);
                JSONObject result = new JSONObject();
                return HandResult.SUCCESS(result);
            }
        } catch (IOException e) {
            e.printStackTrace();
            return HandResult.FAIL();
        }
    }

    @Override
    public HandResult unZip(String zipFile, String saveDir) throws Exception {
        OperationFile operationFile = new OperationFile();
        operationFile.unZip(zipFile, saveDir);
        return null;
    }

    @Override
    public HandResult init(String tableName, String shpDir) throws Exception {
        OperationFile operationFile = new OperationFile();
        List<String> fileNames = operationFile.getFileName(shpDir);
        for (String fileName : fileNames) {
            if (fileName.endsWith(".shp")) {
                String shpFile = shpDir + File.separator + fileName;
                initShp.produce(tableName, shpFile);
            }
        }
        return HandResult.SUCCESS();
    }

    @Override
    public HandResult clipRaster(String layer, String tableName, int gid, double pixelX, double pixelY) {
        clipRaster.clip(layer, tableName, gid, pixelX, pixelY);
        return null;
    }

    private String date() {
        Date date = new Date();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMddhhmmss");
        String formattedDate = dateFormat.format(date);
        return formattedDate;
    }

    private void cleanFile(OperationFile operationFile, String fileDir) throws Exception {
        File folder = new File(fileDir);
        if (folder.exists()) {
            operationFile.deleteFolder(folder);
        }
        folder.mkdir();
    }

    /**
     * 判断文件类型是否存在
     *
     * @param fileNames
     * @param type
     * @return
     */
    private void lack(List<String> fileNames, String type, List<String> lackTypes) {
        boolean lack = true;
        for (String fileName : fileNames) {
            if (fileName.endsWith(type)) {
                lack = false;
            }
        }
        if (lack == true) {
            lackTypes.add(type);
        }
    }
}
