package com.free.fs.controller;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

public class NewImageUtils {

    /**
     * @param file      源文件(图片)
     * @param centerX 图标中心点x像素坐标
     * @param centerY 图标中心点y像素坐标
     * @param alpha     透明度, 选择值从0.0~1.0: 完全透明~完全不透明
     * @return BufferedImage
     * @throws IOException
     * @Title: 构造图片
     * @Description: 生成水印并返回java.awt.image.BufferedImage
     */
    private static BufferedImage draw(File file, int centerX, int centerY, float alpha) throws IOException { //本地图片
        // 获取底图
        BufferedImage buffImg = ImageIO.read(file);
        // 创建Graphics2D对象，用在底图对象上绘图
        Graphics2D g2d = buffImg.createGraphics();
        //消除文字锯齿
        g2d.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        //消除画图锯齿
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        // 在图形和图像中实现混合和透明效果
        g2d.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_ATOP, alpha));

        // 绘制
        compass(g2d, centerX, centerY);
        test(g2d);
        // 释放图形上下文使用的系统资源
        g2d.dispose();

        return buffImg;

    }

    private static void test(Graphics2D g2d) {
        g2d.setColor(Color.BLACK);
        // 3. 两点绘制线段（设置线宽为5px）: 点(50, 150), 点(200, 150)
        BasicStroke bs1 = new BasicStroke(3);       // 笔画的轮廓（画笔宽度/线宽为5px）
        g2d.setStroke(bs1);
        g2d.drawLine(20, 150, 220, 150);

        // 4. 绘制虚线: 将虚线分为若干段（ 实线段 和 空白段 都认为是一段）, 实线段 和 空白段 交替绘制,
        //             绘制的每一段（包括 实线段 和 空白段）的 长度 从 dash 虚线模式数组中取值（从首
        //             元素开始循环取值）, 下面数组即表示每段长度分别为: 5px, 10px, 5px, 10px, ...
        g2d.setColor(Color.WHITE);
        float[] dash = new float[]{25, 25, 50};
        BasicStroke bs2 = new BasicStroke(
                3,                        //画笔宽度/线宽
                BasicStroke.CAP_BUTT,           //设置起点终点样式
                BasicStroke.JOIN_MITER,         //这个参数表示当两条线连接时，连接处的形状
                10.0f,
                dash,                   // 虚线模式数组
                0.0f
        );
        g2d.setStroke(bs2);
        g2d.drawLine(44, 150, 170, 150);
    }

    /**
     * 绘制指北针
     *
     * @param g2d
     * @param centerX 图标中心点x像素坐标
     * @param centerY 图标中心点y像素坐标
     */
    private static void compass(Graphics2D g2d, int centerX, int centerY) {
        //图标放大倍数
        int scale = 5;
        int[] xPoints1 = {0 * scale + centerX, -3 * scale + centerX, 0 * scale + centerX};
        int[] yPoints1 = {-4 * scale + centerY, 7 * scale + centerY, 2 * scale + centerY};
        int[] xPoints2 = {0 * scale + centerX, 0 * scale + centerX, 3 * scale + centerX};
        int[] yPoints2 = {-4 * scale + centerY, 2 * scale + centerY, 7 * scale + centerY};
        // 使用fillPolygon方法绘制实心三角形
        g2d.setColor(Color.BLACK);
        g2d.fillPolygon(xPoints1, yPoints1, 3);
        g2d.setColor(Color.white);
        g2d.fillPolygon(xPoints2, yPoints2, 3);
        g2d.setColor(Color.BLACK);
        g2d.drawPolygon(xPoints2, yPoints2, 3);
        //绘制文字
        Font font = new Font("Arial", Font.PLAIN, 24);
        g2d.setFont(font);
        g2d.drawString("N", centerX - 9, -4 * scale + centerY - 5);
    }

    /**
     * 输出水印图片
     *
     * @param buffImg  图像加水印之后的BufferedImage对象
     * @param savePath 图像加水印之后的保存路径
     */
    private static void generateWaterFile(BufferedImage buffImg, String savePath) {
        int temp = savePath.lastIndexOf(".") + 1;
        try {
            ImageIO.write(buffImg, savePath.substring(temp), new File(savePath));
        } catch (IOException e1) {
            e1.printStackTrace();
        }
    }

    /**
     * 测试本地
     *
     * @param args
     */
    public static void main(String[] args) throws IOException {
        //底层图片地址
        String sourceFilePath = "/data/png/raster.png";
        //合成后的图片地址
        String saveFilePath = "/data/png/result.png";
        // 构建叠加层(alpha 必须是范围 [0.0, 1.0] 之内(包含边界值)的一个浮点数字,可以用来设置透明度)
        int centerX = 220;
        int centerY = 56;
        BufferedImage buffImg = draw(new File(sourceFilePath), centerX, centerY, 1.0f);//本地图片
        generateWaterFile(buffImg, saveFilePath);
    }
}

