package com.free.fs.controller;

import com.free.fs.utils.MBTilesUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.IOException;

@RestController
@RequestMapping("/MapServer")
public class MapPublicController {

    @GetMapping("raster/{layer}/{level}/{col}/{row}")
    public void raster(@PathVariable String layer, @PathVariable String level, @PathVariable String col,
                    @PathVariable String row, HttpServletResponse response) {
        String path = "E:\\data\\mbtiles";
        try {
            if (StringUtils.isEmpty(path)) {
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                return;
            }
            String mbtu = String.format("%s/%s.mbtiles", path, layer);
            File mbtilesFile = new File(mbtu);
            if (!mbtilesFile.exists()) {
                response.setStatus(HttpServletResponse.SC_NOT_FOUND);
                return;
            }
            int z;
            int y;
            int x;
            try {
                z = Integer.valueOf(level);
                x = Integer.valueOf(col);
                y = Integer.valueOf(row.replace(".png", ""));
            } catch (NumberFormatException e) {
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                return;
            }
            MBTilesUtils mbTilesUtils = new MBTilesUtils(mbtu);
            byte[] tile = mbTilesUtils.getTiles(x, y, z);
            if (tile == null) {
                response.setStatus(HttpServletResponse.SC_NOT_FOUND);
                return;
            }
            response.setContentType("image/png");
            response.setContentLength(tile.length);
            ServletOutputStream oStream = response.getOutputStream();
            IOUtils.write(tile, oStream);
            oStream.flush();
            oStream.close();
        } catch (IOException e) {
            System.out.println(String.format("%s/%d/%d/%s请求瓦片出错!", layer, level, col, row));
        }
    }

    @GetMapping("vector/{layer}/{level}/{col}/{row}")
    public void vector(@PathVariable String layer, @PathVariable String level, @PathVariable String col,
                    @PathVariable String row, HttpServletResponse response) {
        String path = "E:\\data\\mbtiles";
        try {
            if (StringUtils.isEmpty(path)) {
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                return;
            }
            String mbtu = String.format("%s/%s.mbtiles", path, layer);
            File mbtilesFile = new File(mbtu);
            if (!mbtilesFile.exists()) {
                response.setStatus(HttpServletResponse.SC_NOT_FOUND);
                return;
            }
            int z;
            int y;
            int x;
            try {
                z = Integer.valueOf(level);
                x = Integer.valueOf(col);
                y = Integer.valueOf(row.replace(".pbf", ""));
            } catch (NumberFormatException e) {
                response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
                return;
            }
            MBTilesUtils mbTilesUtils = new MBTilesUtils(mbtu);
            byte[] tile = mbTilesUtils.getTiles(x, y, z);
            if (tile == null) {
                response.setStatus(HttpServletResponse.SC_NOT_FOUND);
                return;
            }
            response.setContentType("application/x-protobuf");
            response.setHeader("Content-Encoding", "gzip");
            response.setContentLength(tile.length);

            ServletOutputStream oStream = response.getOutputStream();
            IOUtils.write(tile, oStream);
            oStream.flush();
            oStream.close();
        } catch (IOException e) {
            System.out.println(String.format("%s/%d/%d/%s请求瓦片出错!", layer, level, col, row));
        }
    }
}
