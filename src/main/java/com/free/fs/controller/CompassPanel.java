package com.free.fs.controller;

import javax.swing.*;
import java.awt.*;

public class CompassPanel extends JPanel {
    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);
        Graphics2D g2d = (Graphics2D) g;

        // 绘制背景
        g2d.setColor(Color.WHITE);
        g2d.fillRect(0, 0, getWidth(), getHeight());

        // 绘制指北针
        int side = Math.min(getWidth(), getHeight());
        int x = (getWidth() - side) / 2;
        int y = (getHeight() - side) / 2;
        g2d.translate(x + side / 2, y + side / 2);
        g2d.setColor(Color.BLACK);
        g2d.drawLine(0, 0, side / 2, 0); // 中心点
        g2d.fillOval(side / 2 - 5, 0, 10, 10);
        g2d.drawLine(side / 2, 0, side / 2, side / 6); // 指针头

        // 指北针指针
        double angle = Math.toRadians(30); // 指针角度
        int length = side / 4; // 指针长度
        int x2 = (int) (length * Math.sin(angle));
        int y2 = (int) (length * -Math.cos(angle));
        g2d.drawLine(side / 2, 0, side / 2 + x2, y2);
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("Compass");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.add(new CompassPanel());
        frame.setSize(200, 200);
        frame.setVisible(true);
    }
}
