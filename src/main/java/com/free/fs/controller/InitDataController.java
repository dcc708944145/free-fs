package com.free.fs.controller;

import com.free.fs.common.response.HandResult;
import com.free.fs.service.InitDataImpl;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping("/util")
public class InitDataController {
    @Autowired
    InitDataImpl initData;

    @ApiOperation("shp压缩包上传，zip格式")
    @GetMapping(value = "/upload")
    @ResponseBody
    public HandResult upload(MultipartFile file) throws Exception {
        return initData.uploadZip(file);
    }

    @ApiOperation("对zip压缩包进行解压缩")
    @GetMapping(value = "/unzip")
    @ResponseBody
    public HandResult unzip(String zipDir, String saveDir) throws Exception {
        return initData.unZip(zipDir, saveDir);
    }

    @ApiOperation("shp数据入库")
    @GetMapping(value = "/init/shp")
    @ResponseBody
    public HandResult init(String tableName, String shpDir) throws Exception {
        return initData.init(tableName, shpDir);
    }

    @ApiOperation("裁剪栅格瓦片")
    @GetMapping(value = "/clip/raster")
    @ResponseBody
    public HandResult clipRaster(String layer, String tableName, int gid, double pixelX, double pixelY) throws Exception {
        return initData.clipRaster(layer, tableName, gid, pixelX, pixelY);
    }
}
