package com.free.fs.mapper;

import com.baomidou.mybatisplus.annotation.InterceptorIgnore;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

//mybatisplus多租户使用sql拦截导致的不能识别sql语句问题
@InterceptorIgnore(tenantLine = "true")
public interface InitShpMapper {
    void creatTable(@Param("tableName") String tableName,
                    @Param("heads") List<Map<String, Object>> heads);

    void createSequence(@Param("tableName") String tableName);

    void insert(@Param("tableName") String tableName,
                @Param("key") String key,
                @Param("dataList") List<String> dataList);

    void updateCRS(@Param("tableName") String tableName,
                   @Param("epsg") int epsg);
}
