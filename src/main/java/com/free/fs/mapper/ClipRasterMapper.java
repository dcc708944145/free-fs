package com.free.fs.mapper;

import org.apache.ibatis.annotations.Param;

public interface ClipRasterMapper {
    String box(@Param("tableName") String tableName, @Param("gid") Integer gid);
}
