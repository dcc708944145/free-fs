package com.free.fs.common.tools;

import lombok.extern.slf4j.Slf4j;

import java.io.*;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;

import static cn.hutool.core.io.FileUtil.copyFile;


@Slf4j
public class OperationFile {

    //遍历文件夹获取当前文件夹下的文件名
    public List<String> getFileName(String originPath) {
        File file = new File(originPath);
        File[] array = file.listFiles();
        List<String> fileNameList = new ArrayList<>();
        for (int i = 0; i < array.length; i++) {
            if (array[i].isFile()) {
                fileNameList.add(array[i].getName());
            } else if (array[i].isDirectory()) {
                getFileName(array[i].getPath());
            }
        }
        return fileNameList;
    }

    public void getFileName(String originPath, List<String> fileNameList) {
        File file = new File(originPath);
        File[] array = file.listFiles();
        for (int i = 0; i < array.length; i++) {
            if (array[i].isFile()) {
                fileNameList.add(array[i].getName());
            } else if (array[i].isDirectory()) {
                getFileName(array[i].getPath(), fileNameList);
            }
        }
    }

    public void getFiles(String originPath, List<File> fileList) {
        File file = new File(originPath);
        File[] array = file.listFiles();
        for (int i = 0; i < array.length; i++) {
            if (array[i].isFile()) {
                fileList.add(new File(array[i].getPath()));
            } else if (array[i].isDirectory()) {
                fileList.add(new File(array[i].getPath()));
                getFiles(array[i].getPath(), fileList);
            }
        }
    }

    public void deleteAllFile(String originPath) {
        File file = new File(originPath);
        File[] array = file.listFiles();
        for (int i = 0; i < array.length; i++) {
            if (array[i].isFile()) {
                array[i].delete();
            } else if (array[i].isDirectory()) {
                deleteAllFile(array[i].getPath());
            }
        }
    }

    //复制文件夹
    public void copyFolder(String orginPath, String targetPath) {
        File[] files = new File(orginPath).listFiles();
        for (File file : files) {
            if (file.isFile()) {// 如果是文件
                File targetFile = new File(targetPath + File.separator + file.getName());
                // 如果文件已经存在则跳过
                if (targetFile.exists()) {
                    continue;
                } else {
                    // 否则复制
                    try {
                        copyFile(file.getAbsolutePath(), targetFile.getAbsolutePath());
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
            if (file.isDirectory()) {// 如果是目录,就递归
                String childMPath = targetPath + File.separator + file.getName();
                new File(childMPath).mkdir();
                copyFolder(file.getAbsolutePath(), childMPath);
            }
        }
    }

    //复制单个文件
    public void copySingleFile(String orginPath, String targetPath) {
        File file = new File(orginPath);
        File targetFile = new File(targetPath);
        try {
            copyFile(file.getAbsolutePath(), targetFile.getAbsolutePath());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    //修改文件名
    public void rename(String oldFile, String newFile) {
        File file = new File(oldFile);
        if (!file.isFile()) {
            try {
                file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        file.renameTo(new File(newFile));
    }

    public List<String> getFolder(String originPath) {
        File file = new File(originPath);
        File[] array = file.listFiles();
        List<String> filePathList = new ArrayList<>();
        for (int i = 0; i < array.length; i++) {
            if (array[i].isDirectory()) {
                filePathList.add(array[i].getPath());
            }
        }
        return filePathList;
    }

    public List<String> getFolderNames(String originPath) {
        File file = new File(originPath);
        File[] array = file.listFiles();
        List<String> filePathList = new ArrayList<>();
        for (int i = 0; i < array.length; i++) {
            if (array[i].isDirectory()) {
                filePathList.add(array[i].getName());
            }
        }
        return filePathList;
    }

//    public int getFileSize(String path) {
//        File file = new File(path);
//        int size = file.listFiles().length;
//        return size;
//    }

    public void create(String Path) {
        File file = new File(Path);
        if (!file.exists()) {
            file.getParentFile().mkdirs();
        } else {
            file.delete();
        }
    }

    public void deleteFolder(File folder) throws Exception {
        if (!folder.exists()) {
            return;
        }
        File[] files = folder.listFiles();
        if (files != null) {
            for (File file : files) {
                if (file.isDirectory()) {
                    //递归直到目录下没有文件
                    deleteFolder(file);
                } else {
                    //删除
                    file.delete();
                }
            }
        }
        folder.delete();
        folder.mkdirs();
    }

    //如果文件存在则删除
    public void deleteFile(String path) {
        File file = new File(path);
        if (file.exists() == true) {
            file.delete();
        }
    }

    //判断文件是否存在，如果不存在则创建
    public void createFile(String Path) throws IOException {
        File myPath = new File(Path);
        if (!myPath.exists()) {//若此目录不存在，则创建之
            myPath.mkdirs();
        }
    }

    public void judgeFile(String Path) throws IOException {
        File myPath = new File(Path);
        if (!myPath.exists()) {//若此目录不存在，则创建之
        }
    }

    //先创建再往文件中逐条写入数据及追加的形式
    public void writeJson(String filePath, String content) throws IOException {
        FileOutputStream fos = new FileOutputStream(filePath, true);
        PrintWriter pw = new PrintWriter(fos);
        pw.write(content);
        pw.flush();
        pw.close();
        fos.close();
    }

    public void writeObj(String filePath, String content) throws IOException {
        String aContent = content + "\r\n";
        String cleanedContent = removeSpacesAndEmptyLines(aContent.toString());
        FileOutputStream fos = new FileOutputStream(filePath, true);
        PrintWriter pw = new PrintWriter(fos);
        pw.write(cleanedContent);
        pw.flush();
        pw.close();
        fos.close();
    }

    private static String removeSpacesAndEmptyLines(String text) {
        // 去除空格
//        Pattern spacePattern = Pattern.compile("\\s+");
//        Matcher spaceMatcher = spacePattern.matcher(text);
//        text = spaceMatcher.replaceAll("");
        // 去除空行
        Pattern emptyLinePattern = Pattern.compile("^\\s*$", Pattern.MULTILINE);
        Matcher emptyLineMatcher = emptyLinePattern.matcher(text);
        text = emptyLineMatcher.replaceAll("");
        return text;
    }

    public void writeTxt(String filePath, String content) throws IOException {
        String aContent = content + "\r\n";
        FileOutputStream fos = new FileOutputStream(filePath);
        OutputStreamWriter osw = new OutputStreamWriter(fos, StandardCharsets.ISO_8859_1);
        // 写入内容到文件
        osw.write(aContent);
        // 关闭字符流
        osw.close();
    }


    //文件写入
    public void write(String file, String content) throws IOException {
        //将写入转化为流的形式
        BufferedWriter bw = new BufferedWriter(new FileWriter(file));
        //一次写一行
        bw.write(content);
        bw.newLine();  //换行用
        //关闭流
        bw.close();
    }

    public static void createFolder(String folder) {
        File folderFile = new File(folder);
        if (!folderFile.exists()) {
            folderFile.mkdir();
        }
    }

    //计算文件夹大小
    public long getDirSize(File fileDir) {
        File[] files = fileDir.listFiles();
        if (files == null) {
            return 0;
        }
        long fileSize = 0;
        for (File file : files) {
            if (file.isFile()) {
                fileSize += file.length();
            } else {
                fileSize += getDirSize(file);
            }
        }
        return fileSize;
    }

    //计算文件大小
    public long getFileSize(File file) {
        return file.length();
    }

    public void fileExtract(String inputFolder, String outputFolder, String filenames) throws IOException {
        createFolder(outputFolder);
        String[] files = filenames.split(",");
        for (String file : files) {
            String inFile = String.format("%s/%s.geojson", inputFolder, file);
            if (new File(inFile).exists()) {
                String outFile = String.format("%s/%s.geojson", outputFolder, file);
                File tmpFile = new File(inFile);
                if (inFile.length() > 5000 || readTxt(tmpFile).contains("coordinates") == true) {
                    copyFile(inFile, outFile);
                }
            }
        }
    }

    //读取.text文件
    public static String readTxt(File file) {
        StringBuilder result = new StringBuilder();
        try {
            BufferedReader br = new BufferedReader(new FileReader(file));//构造一个BufferedReader类来读取文件
            String s = null;
            while ((s = br.readLine()) != null) {//使用readLine方法，一次读一行
                result.append(System.lineSeparator() + s);
            }
            br.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result.toString();
    }

    //读取.json文件
    public static String readJsonFile(String fileName) {
        String jsonStr = "";
        try {
            File jsonFile = new File(fileName);
            FileReader fileReader = new FileReader(jsonFile);
            Reader reader = new InputStreamReader(new FileInputStream(jsonFile), "utf-8");
            int ch = 0;
            StringBuffer sb = new StringBuffer();
            while ((ch = reader.read()) != -1) {
                sb.append((char) ch);
            }
            fileReader.close();
            reader.close();
            jsonStr = sb.toString();
            return jsonStr;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    //读取zip文件内的文件,返回文件名称列表
    public List<String> readZipFileName(String path) {
        List<String> list = new ArrayList<>();
        try {
            ZipFile zipFile = new ZipFile(path);
            Enumeration<? extends ZipEntry> entries = zipFile.entries();
            while (entries.hasMoreElements()) {
                list.add(entries.nextElement().getName());
            }
            zipFile.close();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (IllegalArgumentException e) {
            e.printStackTrace();
        }
        return list;
    }

    //读取zip文件内的文件,返回文件内容列表
    public static List<String> readZipFile(String path) {
        List<String> list = new ArrayList<>();
        List<List<String>> ddlList = null;
        try {
            ZipFile zipFile = new ZipFile(path);
            InputStream in = new BufferedInputStream(new FileInputStream(path));
            ZipInputStream zin = new ZipInputStream(in);
            ZipEntry ze;
            while ((ze = zin.getNextEntry()) != null) {
                ddlList = new ArrayList<>();
                if (ze.isDirectory()) {
                } else {
                    System.err.println("file - " + ze.getName() + " : " + ze.getSize() + " bytes");
                    long size = ze.getSize();
                    if (size > 0) {
                        BufferedReader br = new BufferedReader(new InputStreamReader(zipFile.getInputStream(ze), Charset.forName("gbk")));
                        String line;
                        while ((line = br.readLine()) != null) {
                            String[] index = line.split(",");
                            List<String> indexList = Arrays.asList(index);
                            ddlList.add(indexList);
                        }
                        br.close();
                    }
                }
                //处理ddlList,此时ddlList为每个文件的内容,while每循环一次则读取一个文件
            }
            zin.closeEntry();
        } catch (IOException e) {
            e.printStackTrace();
        }
        //此处返回无用,懒得修改了
        return list;
    }

    /**
     * 解压.zip文件
     *
     * @param zipFilePath       zip文件路径
     * @param extractFolderPath 保存路径
     */
    public void unZip(String zipFilePath, String extractFolderPath) {
        try (ZipInputStream zipInputStream = new ZipInputStream(Files.newInputStream(Paths.get(zipFilePath)))) {
            File extractFolder = new File(extractFolderPath);
            extractFolder.mkdirs();
            ZipEntry entry;
            while ((entry = zipInputStream.getNextEntry()) != null) {
                Path entryPath = Paths.get(extractFolderPath, entry.getName());

                if (entry.isDirectory()) {
                    Files.createDirectories(entryPath);
                } else {
                    Files.createDirectories(entryPath.getParent());

                    try (OutputStream outputStream = Files.newOutputStream(entryPath)) {
                        byte[] buffer = new byte[4096];
                        int bytesRead;
                        while ((bytesRead = zipInputStream.read(buffer)) != -1) {
                            outputStream.write(buffer, 0, bytesRead);
                        }
                    }
                }

                // 设置文件的最后修改时间，可选
                Files.setLastModifiedTime(entryPath, entry.getLastModifiedTime());
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
