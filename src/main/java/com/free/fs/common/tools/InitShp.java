package com.free.fs.common.tools;

import com.free.fs.common.job.XxlUtils;
import com.free.fs.mapper.InitShpMapper;
import lombok.extern.slf4j.Slf4j;
import org.geotools.data.FeatureSource;
import org.geotools.data.shapefile.ShapefileDataStore;
import org.geotools.data.store.ContentFeatureSource;
import org.geotools.feature.FeatureCollection;
import org.geotools.feature.FeatureIterator;
import org.geotools.referencing.CRS;
import org.locationtech.jts.geom.Geometry;
import org.opengis.feature.Feature;
import org.opengis.feature.Property;
import org.opengis.feature.simple.SimpleFeature;
import org.opengis.feature.simple.SimpleFeatureType;
import org.opengis.feature.type.AttributeDescriptor;
import org.opengis.feature.type.AttributeType;
import org.opengis.referencing.FactoryException;
import org.opengis.referencing.crs.CoordinateReferenceSystem;
import org.opengis.referencing.operation.TransformException;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.sql.SQLException;
import java.util.*;

@Slf4j
@Service
public class InitShp {
    @Resource
    InitShpMapper initShpMapper;
    private int epsg;

    /**
     * 将shp数据入库，坐标为4326
     * @param tableName
     * @param shpFile
     * @throws IOException
     * @throws SQLException
     * @throws TransformException
     * @throws FactoryException
     */
    public void produce(String tableName, String shpFile) throws IOException, SQLException, TransformException, FactoryException {
        File subFile = new File(shpFile);
        List<Map<String, Object>> heads = heads(subFile);
        XxlUtils.log(log, "开始创建数据库表...");
        createTable(tableName, heads);
        XxlUtils.log(log, "数据库表创建成功...");
        XxlUtils.log(log, "开始创建序列...");
        createSequence(tableName);
        XxlUtils.log(log, "创建序列成功...");
        XxlUtils.log(log, "开始导入数据...");
        doInsert(tableName, shpFile, heads);
        updateCRS(tableName);
    }

    public List<Map<String, Object>> heads(File subFile) throws IOException {
        ShapefileDataStore shpDataStore = new ShapefileDataStore(subFile.toURI().toURL());
        shpDataStore.setCharset(Charset.forName("GBK"));
//        shpDataStore.setCharset(Charset.forName("UTF-8"));
        // 获取地图要素
        FeatureSource<SimpleFeatureType, SimpleFeature> featureSource = shpDataStore.getFeatureSource();
        //获取头属性信息
        List<AttributeDescriptor> attributeDescriptors = featureSource.getSchema().getAttributeDescriptors();
        List<Map<String, Object>> heads = new ArrayList<>();
        for (AttributeDescriptor attributeDescriptor : attributeDescriptors) {
            Map<String, Object> et = new HashMap<>();
            String name = attributeDescriptor.getName().toString().toLowerCase();
            String pgType = null;
            if (name.equals("the_geom")) {
                pgType = "geometry";
            } else {
                AttributeType type = attributeDescriptor.getType();
                String className = type.getBinding().getName();
                String shpType = className.substring(className.lastIndexOf(".") + 1);
                //转数据库类型
                if (shpType.equals("Integer")) {
                    pgType = "int";
                } else if (shpType.equals("Double")) {
                    pgType = "float8";
                } else {
                    pgType = "varchar";
                }
            }
            et.put("name", name);
            et.put("type", pgType);
            heads.add(et);
        }
        return heads;
    }

    public void doInsert(String pgTableName, String shpPath, List<Map<String, Object>> heads) throws IOException, SQLException, TransformException, FactoryException {
        ShapefileDataStore shapefileDataStore = null;
        FeatureIterator featureIterator = null;
        shapefileDataStore = new ShapefileDataStore(new File(shpPath).toURI().toURL());
        shapefileDataStore.setCharset(Charset.forName("GBK"));
        if (shapefileDataStore == null) {
            XxlUtils.log(log, shpPath + "加载失败！");
            return;
        }
        String typeName = shapefileDataStore.getTypeNames()[0];
        ContentFeatureSource featureSource = shapefileDataStore.getFeatureSource();
//        CoordinateReferenceSystem crs = featureSource.getInfo().getCRS();
        SimpleFeatureType schema = shapefileDataStore.getSchema(typeName);
        CoordinateReferenceSystem srs = schema.getCoordinateReferenceSystem();
        //当src为空时说明shp数据未定义坐标系
        if (srs == null) {
            return;
        }
        epsg = CRS.lookupEpsgCode(srs, true);

        FeatureCollection featureCollection = shapefileDataStore.getFeatureSource(typeName).getFeatures();
        featureIterator = featureCollection.features();
        int i = 1;
        List<Map<String, Object>> insertList = new ArrayList<>();
        while (featureIterator.hasNext()) {
            Feature feature = featureIterator.next();
            Iterator<Property> iterator = feature.getProperties().iterator();
            Map<String, Object> insertParams = new HashMap<>();
            Geometry geom = (Geometry) feature.getDefaultGeometryProperty().getValue();
            insertParams.put("the_geom", geom);
            /**
             * 去除数据中geometry为空的数据
             **/
            if (geom == null) {
                continue;
            }
            while (iterator.hasNext()) {
                Property property = iterator.next();
                if (property.getValue() == null) {
                    insertParams.put(property.getName().toString().toLowerCase(), null);
                } else {
                    if (!"the_geom".equals(property.getName())) {
                        if (property.getValue().toString().contains("'")) {
                            String value = property.getValue().toString().replace("'", "2");
                            insertParams.put(property.getName().toString().toLowerCase(), value);
                        } else {
                            insertParams.put(property.getName().toString().toLowerCase(), property.getValue());
                        }
                    }
                }
            }
            insertList.add(insertParams);
            if (i % 2000 == 0) { // 每2000条插入一次
                insertData(pgTableName, insertList, heads);
                insertList.clear();
                XxlUtils.log(log, "数据库表:{},已插入{}行", pgTableName, i);
            }
            i++;
        }
        if (insertList.size() > 0) { // 剩余数据插入
            insertData(pgTableName, insertList, heads);
            XxlUtils.log(log, "数据库表:{},已插入{}行", pgTableName, i - 1);
        }
    }

    public void insertData(String tableName, List<Map<String, Object>> insertList, List<Map<String, Object>> heads) throws SQLException {
        List<String> dataList = new ArrayList<>();
        for (int i = 0; i < insertList.size(); i++) {
            StringBuilder keys = new StringBuilder();
            StringBuilder values = new StringBuilder();
            Map<String, Object> map = insertList.get(i);
            for (int j = 0; j < heads.size(); j++) {
                Map<String, Object> head = heads.get(j);
                String key = head.get("name").toString();
                String value = "";
                if (map.get(key) != null) {
                    value = map.get(key).toString();
                }
                keys.append("\"" + key + "\"");
                if (value == "") {
                    values.append("null");
                } else {
                    values.append("'" + value + "'");
                }
                if (j != heads.size() - 1) {
                    keys.append(",");
                    values.append(",");
                }
            }
            dataList.add(values.toString());
            initShpMapper.insert(tableName, keys.toString(), dataList);
        }
    }

    public void createTable(String tableName, List<Map<String, Object>> heads) {
        initShpMapper.creatTable(tableName, heads);
    }

    public void createSequence(String tableName) {
        initShpMapper.createSequence(tableName);
    }

    public void updateCRS(String tableName) {
        initShpMapper.updateCRS(tableName, epsg);
    }
}
