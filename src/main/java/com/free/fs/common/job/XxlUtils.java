package com.free.fs.common.job;

import com.alibaba.fastjson.JSONObject;
import com.xxl.job.core.context.XxlJobHelper;
import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;

/**
 * @Author DengChengCai
 * @Date 14:27 2022/6/24
 **/
public class XxlUtils {
    /**
     * 获取任务启动参数，并转为对应的对象
     */
    public static <T> T getJobParamObject(Class<T> clazz) {
        String param = XxlJobHelper.getJobParam();
        if (StringUtils.isNotEmpty(param)) {
            return JSONObject.parseObject(param, clazz);
        } else {
            return null;
        }
    }

    /**
     * 获取任务启动参数，并转为jsonObject
     */
    public static JSONObject getJobParamJson() {
        String param = XxlJobHelper.getJobParam();
        if (StringUtils.isNotEmpty(param)) {
            return JSONObject.parseObject(param);
        } else {
            return new JSONObject();
        }
    }

    public static void error(String msg) {
        XxlJobHelper.log(msg);
        throw new RuntimeException(msg);
    }

    public static void log(Logger log, String appendLogPattern, Object... appendLogArguments) {
        log.info(appendLogPattern, appendLogArguments);
        XxlJobHelper.log(appendLogPattern, appendLogArguments);
    }
}
