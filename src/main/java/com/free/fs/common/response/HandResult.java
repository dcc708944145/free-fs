package com.free.fs.common.response;

import com.free.fs.enums.ResultEnum;
import lombok.Data;

@Data
public class HandResult {
    private String message;
    private boolean result;
    private Object data;
    private ResultEnum code;
    private String id;

    public static HandResult SUCCESS(Object data) {
        HandResult result = new HandResult();
        result.setData(data);
        result.setResult(true);
        result.setCode(ResultEnum.RESULT_SUCCESS);
        result.setMessage("成功！");
        return result;
    }

    public static HandResult SUCCESS() {
        HandResult result = new HandResult();
        result.setResult(true);
        result.setCode(ResultEnum.RESULT_SUCCESS);
        result.setMessage("成功！");
        return result;
    }

    public static HandResult SUCCESS(Object data, String message) {
        HandResult result = new HandResult();
        result.setData(data);
        result.setResult(true);
        result.setCode(ResultEnum.RESULT_SUCCESS);
        result.setMessage(message);
        return result;
    }


    public static HandResult FAIL(String message) {
        HandResult result = new HandResult();
        result.setResult(false);
        result.setCode(ResultEnum.RESULT_FAIL);
        result.setMessage(message);
        return result;
    }

    public static HandResult FAIL() {
        HandResult result = new HandResult();
        result.setResult(false);
        result.setCode(ResultEnum.RESULT_FAIL);
        result.setMessage("成功");
        return result;
    }
}
