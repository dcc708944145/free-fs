package com.free.fs.utils;

import java.io.File;
import java.sql.*;
import java.util.concurrent.ConcurrentHashMap;

public class MBTilesUtils {

    private static final ConcurrentHashMap<String, MBTilesUtils> INSTANCES = new ConcurrentHashMap<String, MBTilesUtils>();

    private final Connection conn;

    private final PreparedStatement ps;

    public MBTilesUtils(String db) {
        try {
            Class.forName("org.sqlite.JDBC");
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }

        if (db == null || !new File(db).exists()) {
            throw new RuntimeException("No database");
        }

        try {
            conn = DriverManager.getConnection("jdbc:sqlite:" + db);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        try {
            ps = conn.prepareStatement("SELECT tile_data FROM tiles "
                    + "WHERE zoom_level = ? AND tile_column = ? "
                    + "AND tile_row = ?;");
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public synchronized byte[] getTiles(int x, int y, int z) {
        int index = 1;

        ResultSet rs = null;
        try {
            ps.setInt(index++, z);
            ps.setInt(index++, x);
            ps.setInt(index++, y);

            rs = ps.executeQuery();
            if (rs.next()) {
                return rs.getBytes(1);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return null;
    }
}
