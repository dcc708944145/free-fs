package com.free.fs.utils.raster;


import org.geotools.geometry.jts.JTS;
import org.geotools.geometry.jts.JTSFactoryFinder;
import org.locationtech.jts.geom.Coordinate;
import org.locationtech.jts.geom.Envelope;
import org.locationtech.jts.geom.Geometry;
import org.locationtech.jts.geom.GeometryFactory;
import org.locationtech.jts.io.ParseException;
import org.locationtech.jts.io.WKTReader;

import java.io.File;
import java.sql.SQLException;
import java.util.List;

/**
 * @Author DengChengCai
 * @Date 2022/5/11
 */

public class TileRange {
    private int TILE_SIZE;
    private Geometry[] polygons;
    private double px;
    private double py;
    private int zoom;

    public TileRange(int tileSize) {
        this.TILE_SIZE = tileSize;
    }

    public Range toRange(String[] bounds, int z) {
        this.zoom = z;
        Range range = new Range();
        int[] tmp = lonLatToXY(Double.valueOf(bounds[0]), Double.valueOf(bounds[3]), z);
        range.setMinC(tmp[0]);
        range.setMinR(tmp[1]);
        tmp = lonLatToXY(Double.valueOf(bounds[2]), Double.valueOf(bounds[1]), z);
        range.setMaxC(tmp[0]);
        range.setMaxR(tmp[1]);
        return range;
    }

    /**
     * 将4326坐标转换为瓦片坐标
     * @param lon
     * @param lat
     * @param zoom
     * @return
     */
    public int[] lonLatToXY(double lon, double lat, int zoom) {
        int[] xy = new int[2];
        xy[0] = (int) Math.floor(lonToTileX(lon, zoom));
        xy[1] = (int) Math.floor(latToTileY(lat, zoom));
        return xy;
    }

    public long lonToTileX(double lon, int zoom) {
        return pixelXToTileX(lonToPixelX(lon, zoom), zoom);
    }

    public long latToTileY(double lat, int zoom) {
        return pixelYToTileY(latToPixelY(lat, zoom), zoom);
    }

    public double lonToPixelX(double lon, int zoom) {
        return (lon + 180) / 360 * ((long) TILE_SIZE << zoom);
    }

    public double latToPixelY(double lat, int zoom) {
        double sinLat = Math.sin(lat * (Math.PI / 180));
        long mapSize = ((long) TILE_SIZE << zoom);
        // FIXME improve this formula so that it works correctly without the clipping
        double pixelY = (0.5 - Math.log((1 + sinLat) / (1 - sinLat)) / (4 * Math.PI)) * mapSize;
        return Math.min(Math.max(0, pixelY), mapSize);
    }

    public long pixelXToTileX(double pixelX, int zoom) {
        return (long) Math.min(Math.max(pixelX / TILE_SIZE, 0), Math.pow(2, zoom) - 1);
    }

    public long pixelYToTileY(double pixelY, int zoom) {
        return (long) Math.min(Math.max(pixelY / TILE_SIZE, 0), Math.pow(2, zoom) - 1);
    }



//    public String getBounds(String code) throws SQLException, ParseException {
//        //BOX(38567083.49726701 2563278.5416080058,38567325.50317699 2563501.2424550056)
//        String file = String.format("%s/data/image_raster/china84.sqlite", System.getProperty("user.dir"));
//        SqliteHelper sqliteHelper = new SqliteHelper(file);
//        WKTReader reader = new WKTReader(JTSFactoryFinder.getGeometryFactory());
//        String fileName = new File(file).getName();
//        String name = fileName.substring(0, fileName.lastIndexOf("."));
//        String sql = String.format("select WKT_GEOMETRY from %s where code='%s'", name, code);
//        List<String> wkts = sqliteHelper.queryMulti(sql);
//        polygons = new Geometry[wkts.size()];
//        double xMin = 1000;
//        double yMin = 1000;
//        double xMax = -1000;
//        double yMax = -1000;
//        for (int i = 0; i < wkts.size(); i++) {
//            polygons[i] = reader.read(wkts.get(i));
//            Envelope envelope = polygons[i].getEnvelopeInternal();
//            xMin = Math.min(xMin, envelope.getMinX());
//            yMin = Math.min(yMin, envelope.getMinY());
//            xMax = Math.max(xMax, envelope.getMaxX());
//            yMax = Math.max(yMax, envelope.getMaxY());
//        }
//        String bounds = String.format("%s %s %s %s", String.valueOf(xMin), String.valueOf(yMin), String.valueOf(xMax), String.valueOf(yMax));
//        sqliteHelper.close();
//        return bounds;
//    }

    public int transTileRow(int tileRow, int zoom) {
        return (int) Math.pow(2, zoom) - tileRow - 1;
    }
}
