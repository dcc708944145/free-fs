package com.free.fs.utils.raster;

public class Range {
    private int minC;
    private int minR;
    private int maxC;
    private int maxR;

    public Range() {
    }

    public Range(String[] bounds, int z) {
        int[] tmp = googleLonLatToXY(Double.valueOf(bounds[0]), Double.valueOf(bounds[1]), z);
        minC = tmp[0];
        minR = tmp[1];
        tmp = googleLonLatToXY(Double.valueOf(bounds[2]), Double.valueOf(bounds[3]), z);
        maxC = tmp[0];
        maxR = tmp[1];
    }

    public Range(double[] bounds, int z) {
        int[] tmp = googleLonLatToXY(bounds[0], bounds[1], z);
        minC = tmp[0];
        minR = tmp[1];
        tmp = googleLonLatToXY(bounds[2], bounds[3], z);
        maxC = tmp[0];
        maxR = tmp[1];
    }

    private int[] googleLonLatToXY(double lon, double lat, int zoom) {
        double n = Math.pow(2, zoom);
        double tileX = ((lon + 180) / 360) * n;
        double tileY = (1 - (Math.log(Math.tan(Math.toRadians(lat)) + (1 / Math.cos(Math.toRadians(lat)))) / Math.PI)) / 2 * n;
        int[] xy = new int[2];
        xy[0] = (int) Math.floor(tileX);
        xy[1] = (int) Math.floor(tileY);
        int ext = (int) n;
        xy[1] = ext - xy[1] - 1;
        return xy;
    }

    public Range(int minC, int minR, int maxC, int maxR) {
        this.minC = minC;
        this.minR = minR;
        this.maxC = maxC;
        this.maxR = maxR;
    }

    public int getRows() {
        return Math.abs(maxR - minR) + 1;
    }

    public int getCols() {
        return Math.abs(maxC - minC) + 1;
    }

    public int getSize() {
        return (maxC - minC + 1) * (maxR - minR + 1);
    }

    public int getMinC() {
        return minC;
    }

    public void setMinC(int minC) {
        this.minC = minC;
    }

    public int getMinR() {
        return minR;
    }

    public void setMinR(int minR) {
        this.minR = minR;
    }

    public int getMaxC() {
        return maxC;
    }

    public void setMaxC(int maxC) {
        this.maxC = maxC;
    }

    public int getMaxR() {
        return maxR;
    }

    public void setMaxR(int maxR) {
        this.maxR = maxR;
    }
}