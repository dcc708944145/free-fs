package com.free.fs.utils.raster;

import com.free.fs.mapper.ClipRasterMapper;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

@Service
public class ClipRaster {
    @Resource
    ClipRasterMapper clipRasterMapper;

    public void clip(String layer, String tableName, int gid, double pixelX, double pixelY) {
        String box = clipRasterMapper.box(tableName, gid);
        System.out.println(box);
    }
}
